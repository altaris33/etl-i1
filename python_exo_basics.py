# 1)
v = [54, 12, 35]
u = v*5
u

# 2) 
v
v[:-1]
u_2 = v[:-1]*5
u_2

# 3)
v_bool = [-54, 12, 0 , -35]
v_bool 
u_bool = list(map(lambda x: True if x >= 0 else False, v_bool))
u_bool

# 4)
v_pos = [-54, 12, 0, -35]
u_pos = []
for index, value in enumerate(v_pos):
    if(value >=0):
        u_pos.append(index)

u_pos        

# 5)
v_hundred = [54, 12, 35]
u_hundred = [x+y for x in range(0, 10000, 100) for y in v_hundred]
u_hundred  

# 6)

v_letters = ['a', 'b', 'c']
d_letters = {'a': 54, 'b': 0, 'c': -35}
u_letters = [x for x in d_letters.keys()]
u_letters

# 7)

v_letters2 = ['a', 'c', 'x']
d_letters2 = {'a':54, 'b':0, 'c':-35}
u_letters2 = [d_letters2.get(key, -1) for key in v_letters2]

# 8)

v_8 = ['a', 54, 'b', 0, 'c', -35]
d_8 = { value : v_8[v_8.index(value)+1] for value in v_8 if type(value) is str}

# 9)

v_9 = ['25=3+2+x', '12=a+c+4']
d_9 = {c.split("=")[0] : e.split("+") for c in v_9 for e in c.split("=")[1:]}

# 10)

d_10 = { 'a':['x','y'], 'b':['w', 'x', 'z'] } 
v_10 = [[key,values] for key in d_10 for values in d_10[key]]
