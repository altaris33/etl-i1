# Auteur : Jérémie LAERA
# Date: 21/07/2020

# Objectif: Recueillir les données d'un fichier Excel provenant de la plateforme
#           Data.gouv.fr
#           Préparer et normaliser les données
#
# Langage choisi pour les commentaires et nom de variable: anglais.
#
# Indication: le fichier travaillé est perçu depuis le poste local et doit être téléchargé:
#   Il est disponible sur le lien suivant: https://gitlab.com/altaris33/etl-i1/-/blob/master/rna_waldec_20200701_dpt_33.csv
# Ensuite le script python depuis le même emplacement que le fichier csv afin de trouver ce dernier    
# Ce programme a été développé sous l'IDE Spyder (hérité de R Studio)
# Grâce à l'explorateur de variables et la console les print n'ont pas été nécessaires
# Néanmoins dépendant de votre environnement de développement n'hésitez pas à rajouter des prints.


# imports
import os
import requests as rq
import io
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# check out the current working directory
current_working_dir = os.getcwd()

# retrieving the csv file to form a data frame
def get_csv_file():
    """
    
    Returns
    -------
    my_data : data contained in the csv file.

    """
    
    #file_url = "https://gitlab.com/altaris33/etl-i1/-/blob/master/rna_waldec_20200701_dpt_33.csv"
    file_local_url = "rna_waldec_20200701_dpt_33.csv"
    #response = rq.get(file_url)
    
    #file_object = io.StringIO(response.content.decode('ISO-8859-1'))
    my_data = pd.read_csv(file_local_url, sep=';', encoding="ISO-8859-1")
    
    return my_data


# standardization - to be updated 
def create_ids(list):
    return range(1, len(list) + 1)

# returning ids if found in the dataframe


# Creating, analyzing and reading the dataframe
data_csv = get_csv_file()

type(data_csv)
data_csv
data_csv.describe()  

# printing the 5 first rows of the loaded data
data_csv.head(5)

# printing the 2 last rows of the loaded data
data_csv.tail(2)  


# visualizing columns and filtering data
columns = data_csv.columns
# data_csv.adrs_codepostal<33400
# data_csv.id

#------------------------------------------------------------------------------
# simple tables & fields definition
#------------------------------------------------------------------------------
titre_association = data_csv['titres de l\association'] = data_csv[data_csv.columns[11:13]]
objet_association = data_csv['objet_association'] = data_csv[data_csv.columns[13:16]]
adress_asso = data_csv['adresse_association'] = data_csv[data_csv.columns[16:25]]
civilité = data_csv['Civilité'] = data_csv['dir_civilite']
site_web = data_csv['site_web'] = data_csv['siteweb']
observation = data_csv['observation'] = data_csv['observation']
date_creation = data_csv['Date de création'] = pd.to_datetime(data_csv['date_creat'], errors='coerce')
date_declaration = data_csv['Date de déclaration'] = pd.to_datetime(data_csv['date_decla'], errors='coerce')
date_publication = data_csv['Date de publication'] = pd.to_datetime(data_csv['date_publi'], errors='coerce')
date_dissolution = data_csv['Date de dissolution'] = pd.to_datetime(data_csv['date_disso'], errors='coerce')
maj_date = data_csv['Dernière mise à jour'] = pd.to_datetime(data_csv['maj_time'])
asso_with_siret = data_csv['AssociationAvecSiret'] = data_csv['siret'].dropna()

#------------------------------------------------------------------------------
# WORKING WITH IDs
#------------------------------------------------------------------------------
def convert_ids_to_numeric_ids():
    
    """
    Returns
    -------
    ids_to_frame : a Series converted to a Dataframe.
    """
    
    ids = data_csv.id
    ids_to_frame = ids.to_frame()
    return ids_to_frame

change_asso_ids = convert_ids_to_numeric_ids()
# creating ids
# changing alphanumeric ids for associations to numeric, incremental ones
id_association_redefined = change_asso_ids.assign(id=data_csv['id'].astype('category').cat.codes)

# changing the obtained Dataframe back to a Series
id_association_table = id_association_redefined.iloc[:,0]

# Foreign keys
#------------------------------------------------------------------------------
# TO DO
#------------------------------------------------------------------------------


# parameterizing civilite into numeric values for later use
def search_for_all_civilites(gathering):
    """
    

    Parameters
    ----------
    gathering : a panda Series Object to be passed to fetch all data from a specific column.

    Returns
    -------
    gathering : a Series corresponding to all occurrences found.

    """
    
    gathering = data_csv.groupby('dir_civilite').dir_civilite.count() < 2
    return gathering

all_civilite = search_for_all_civilites(data_csv.dir_civilite)

# PORTION NOT WORKING YET, TO BE CORRECTED
numeric_civilite = {'PF':1, 'PM':2, 'SF':3, 'MS':4, 'TS':5, 'CO':6, 'PG':7, 'PC':8, 'TM':9, 'VF':10, 'FS':11, 'CC':12
                    ,'MA':13, 'PL':14, 'MK':15, 'TA':16, 'PH':17, 'AF':18, 'AR':19, 'CF':20, 'CP':21, 'CT':22, 'CV':23
                    ,'DG':24, 'LG':25, 'MC':26, 'MG':27, 'MJ':28, 'MP':29, 'MR':30, 'MS':31, 'PA':32, 'PE':33, 'PP':34
                    ,'PS':35, 'RA':36, 'RE':37, 'RM':38, 'RP':39, 'TJ':40, ' ':40}
categorical_civilite_values = data_csv.dir_civilite = [numeric_civilite[item] for item in data_csv.dir_civilite]
# END OF NON-WORKING PORTION

#------------------------------------------------------------------------------
# Complex table: association
# PORTION NOT WORKING YET, TO BE CORRECTED
# Foreign key still missing 
#------------------------------------------------------------------------------
association  = pd.DataFrame({
    'id' : list(id_association_redefined),
    'nom_association': list(titre_association),
    'siret': list(data_csv['siret']),
    'id_objet': list(data_csv['objet_association'].apply(lambda count: create_ids(objet_association, count))),
    'id_date_creation': list(date_creation.apply(lambda count: create_ids(date_creation, count))),
    'id_date_declaration': list(date_declaration.apply(lambda count: create_ids(date_declaration, count))),
    'id_date_publication': list(date_declaration.apply(lambda count: create_ids(date_declaration, count))),
    'id_date_dissolution': list(date_declaration.apply(lambda count: create_ids(date_declaration, count))),
    }
    , index = id_association_table)

# END OF NON-WORKING PORTION

# display the new column along with ids
ids_and_maj_dates = data_csv[['Dernière mise à jour', 'id']]   

# getting unique elements from column "numero gestion"
unique_gestion_number = data_csv['gestion'].unique()

#-----------------------------------------------------------------------------
# Filter functions
#-----------------------------------------------------------------------------
def filter_by_postal_code():
    
    """
    
    Returns
    -------
    filtered_postal : a filter containing all instance where the postal code
    is less than 33200.

    """
    filtered_postal = data_csv[data_csv.adrs_codepostal<33200]
    
    return filtered_postal

filter_x = filter_by_postal_code()


def get_asso_with_siret():
    """

    Returns
    -------
    found_asso : a filter containing all association with a siret.

    """    
    
    found_asso = data_csv[['AssociationAvecSiret','id']].dropna()
    
    return found_asso

filter_asso_with_siret = get_asso_with_siret()

def select_asso_with_dissolution_date():
    
    """

    Returns
    -------
    found_asso : a filter containing all association with a date of dissolution.

    """ 
    
    found_asso = data_csv[['Date de dissolution', 'id']].dropna()
    
    return found_asso

filter_asso_with_dissolution_date = select_asso_with_dissolution_date()

#------------------------------------------------------------------------------
# Visualisation des données
#------------------------------------------------------------------------------
data_csv.plot(x ='Date de dissolution', y='id', kind = 'scatter')
plt.show()

